package cat.itb.ex4;

import java.util.Arrays;

public class Ex4 {

    public static void main(String[] args) {
        String encryptedMessage = "Asrssnfat tlasocu toec sd tg ret ocs otserrepw ar carhawc ameym v aoi iohcsal et prs si ";
        int clau = 4;
        int column = encryptedMessage.length() / clau;
        String[][] message = new String[column][clau];
        int num = 0;
        for (int i = 0; i < message.length; i++){
            for (int j = 0; j < message[i].length; j++){
                message[i][j] = String.valueOf(encryptedMessage.charAt(num));
                num++;
            }
        }
        for (int i = 0; i < message.length; i++){
            System.out.println(Arrays.toString(message[i]));
        }
    }
}
